import React from "react";

export default class PageVisibilityWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      idle: false,
      remainingTime: 60
    };
  }
  static defaultProps = {
    idleTimeOutMs: 1000 * 1
  };

  componentDidMount() {
    document.addEventListener("visibilitychange", () =>
      this.handleVisibilityChange(this.props.idleTimeOutMs)
    );
  }

  render() {
    return <React.Fragment>{this.props.children}</React.Fragment>;
  }
  handleVisibilityChange = keepAliveTime => {
    if (document.hidden) {
      setIdleTime();
    } else {
      this.timeoutHasEnded("userIdleTimeStamp", keepAliveTime);
    }
  };
  timeoutHasEnded = (storageItem, idleTimeOutLength) => {
    const currentTime = getCurrentTime();
    if (window.localStorage.hasOwnProperty(storageItem)) {
      if (
        currentTime - window.localStorage.getItem(storageItem) >
        idleTimeOutLength
      ) {
        window.localStorage.removeItem(storageItem);
        this.setState({ idle: false, remainingTime: 0 }, () => {
          alert("time to log out");
        });
      }
    }
  };
}

function getCurrentTime() {
  return new Date().getTime();
}

function setIdleTime() {
  const userIdleTimeStamp = getCurrentTime();
  window.localStorage.setItem("userIdleTimeStamp", userIdleTimeStamp);
}
